package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

import database.Producto;
import database.ProductoFunciones;

public class MainActivity extends AppCompatActivity {

    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rgOpciones;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnEditar;
    private Producto saveProducto;
    private ProductoFunciones db;
    private long id;
    private long indexActual;


    ArrayList<Producto> productos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        db = new ProductoFunciones(MainActivity.this);

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtMarca = (EditText)findViewById(R.id.txtMarca);
        txtPrecio = (EditText)findViewById(R.id.txtPrecio);
        rgOpciones = (RadioGroup)findViewById(R.id.rgOpciones);
        btnGuardar = (Button)findViewById(R.id.btnGuardar);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnEditar = (Button)findViewById(R.id.btnEditar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNombre.getText().toString().matches("") || txtMarca.getText().toString().matches("") || txtPrecio.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"Ingrese todos los datos",Toast.LENGTH_SHORT).show();
                }
                else {

                    Producto producto = new Producto();

                    producto.setNombre(txtNombre.getText().toString());
                    producto.setMarca(txtMarca.getText().toString());
                    producto.setPrecio(Double.valueOf(txtPrecio.getText().toString()));
                    if (rgOpciones.getCheckedRadioButtonId() == R.id.rdb1){
                        producto.setPredecedero(1);
                    }
                    else {
                        producto.setPredecedero(0);
                    }
                    if (rgOpciones.getCheckedRadioButtonId() == R.id.rdb2){
                        producto.setNo_predecedero(1);
                    }
                    else {
                        producto.setNo_predecedero(0);
                    }
                    db.openDataBase();
                    if (saveProducto == null){
                        long idx = db.InsertarProducto(producto);
                        Toast.makeText(MainActivity.this,"Se agregó el Registro con ID: " + idx, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        db.UpdateContacto(producto,id);
                        Toast.makeText(MainActivity.this,"se Actualizó el Registro: " + id, Toast.LENGTH_SHORT).show();
                    }
                    db.cerrarDataBase();
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });
        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ProductoActivity.class);
                Bundle bObject = new Bundle();
                db.openDataBase();
                productos = db.allProductos();
                db.cerrarDataBase();
                bObject.putSerializable("productos", productos);
                bObject.putLong("indexActual", indexActual);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });
    }
    public void Limpiar(){
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
    }
}
