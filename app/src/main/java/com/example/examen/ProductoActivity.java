package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

import database.DefinirTabla;
import database.Producto;
import database.ProductoFunciones;

public class ProductoActivity extends AppCompatActivity {

    private EditText txtCodigoB;
    private EditText txtNombreB;
    private EditText txtMarcaB;
    private EditText txtPrecioB;
    private RadioGroup rgOpcionesB;
    private Button btnBorrar;
    private Button btnActualizar;
    private Button btnCerrar;
    private Button btnBuscar;
    private ArrayList<Producto> productos;
    private ProductoFunciones productoFunciones;
    private long position;
    Producto producto = new Producto();
    private SQLiteDatabase db;
    private MainActivity mainActivity;
    private Producto saveObject = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        txtCodigoB = (EditText) findViewById(R.id.txtCodigoB);
        txtNombreB = (EditText)findViewById(R.id.txtNombreB);
        txtMarcaB = (EditText)findViewById(R.id.txtMarcaB);
        txtPrecioB = (EditText)findViewById(R.id.txtPrecioB);
        rgOpcionesB = (RadioGroup)findViewById(R.id.rgOpcionesB);
        btnBorrar = (Button)findViewById(R.id.btnBorrar);
        btnActualizar = (Button)findViewById(R.id.btnActualizar);
        btnCerrar = (Button)findViewById(R.id.btnCerrar);
        btnBuscar = (Button)findViewById(R.id.btnBuscarId);


        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productoFunciones.openDataBase();
                Producto producto = productoFunciones.getProducto(Long.valueOf(txtCodigoB.getText().toString()));
                if(producto == null)
                {
                    Toast.makeText(ProductoActivity.this, "No se encontro producto", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    saveObject = producto;
                    txtMarcaB.setText(saveObject.getMarca());
                    txtNombreB.setText(saveObject.getNombre());
                    txtPrecioB.setText(String.valueOf(saveObject.getPrecio()));
                }
                productoFunciones.cerrarDataBase();
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productoFunciones.openDataBase();
                productoFunciones.DeleteProducto(Long.valueOf(txtCodigoB.getText().toString()));
                Toast.makeText(ProductoActivity.this, "Se elimino el producto", Toast.LENGTH_SHORT).show();
                productoFunciones.cerrarDataBase();
                mainActivity.Limpiar();
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productoFunciones.openDataBase();
                if (txtNombreB.getText().toString().matches("") || txtMarcaB.getText().toString().matches("") || txtPrecioB.getText().toString().matches("")) {
                    Producto producto = new Producto();
                    //producto.setCodigo(Integer.parseInt(getText(txtCodigo)));
                    producto.set_ID(saveObject.get_ID());
                    producto.setNombre(txtNombreB.getText().toString());
                    producto.setPrecio(Double.valueOf(txtPrecioB.getText().toString()));
                    producto.setMarca(txtMarcaB.getText().toString());
                    if (productoFunciones.UpdateContacto(producto, saveObject.get_ID()) != -1)
                        Toast.makeText(ProductoActivity.this, "Se actualizo correctamente", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(ProductoActivity.this, "Se produjo un error", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProductoActivity.this, "No puedes dejar los campos vacios", Toast.LENGTH_SHORT).show();

                }
                mainActivity.Limpiar();
                productoFunciones.cerrarDataBase();
            }

        });


        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
