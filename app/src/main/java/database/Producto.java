package database;

import java.io.Serializable;

public class Producto implements Serializable {

    private long _ID;
    private String nombre;
    private String marca;
    private Double precio;
    private int predecedero;
    private int no_predecedero;

    public Producto(long _ID, String nombre, String marca, Double precio, int predecedero, int no_predecedero) {
        this._ID = _ID;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.predecedero = predecedero;
        this.no_predecedero = no_predecedero;
    }

    public Producto(){

    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public int getPredecedero() {
        return predecedero;
    }

    public void setPredecedero(int predecedero) {
        this.predecedero = predecedero;
    }

    public int getNo_predecedero() {
        return no_predecedero;
    }

    public void setNo_predecedero(int no_predecedero) {
        this.no_predecedero = no_predecedero;
    }
}
