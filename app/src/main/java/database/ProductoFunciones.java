package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ProductoFunciones {

    private Context context;
    private ProductoDbHelper productoDbHelper;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[] {
            DefinirTabla.Producto._ID,
            DefinirTabla.Producto.NOMBRE,
            DefinirTabla.Producto.MARCA,
            DefinirTabla.Producto.PRECIO,
            DefinirTabla.Producto.PREDECEDERO,
            DefinirTabla.Producto.NO_PREDECEDERO
    };


    public ProductoFunciones(Context context) {
        this.context = context;
        productoDbHelper = new ProductoDbHelper(this.context);
    }

    public void openDataBase() {
        db = productoDbHelper.getWritableDatabase();
    }

    public void cerrarDataBase() {
        productoDbHelper.close();
    }

    public long InsertarProducto (Producto p) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.PREDECEDERO, p.getPredecedero());
        values.put(DefinirTabla.Producto.NO_PREDECEDERO, p.getNo_predecedero());

        return db.insert(DefinirTabla.Producto.TABLA_NAME, null, values);
    }

    public long UpdateContacto(Producto p, long id) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.PREDECEDERO, p.getPredecedero());
        values.put(DefinirTabla.Producto.NO_PREDECEDERO, p.getNo_predecedero());

        String criterio = DefinirTabla.Producto._ID + " = " + id;

        return db.update(DefinirTabla.Producto.TABLA_NAME, values, criterio, null);
    }

    public int DeleteProducto(long id) {
        String criterio = DefinirTabla.Producto._ID + " = " + id;
        return db.delete(DefinirTabla.Producto.TABLA_NAME, criterio, null);
    }

    public Producto readProducto(Cursor cursor) {
        Producto producto = new Producto();
        producto.set_ID(cursor.getInt(0));
        producto.setNombre(cursor.getString(1));
        producto.setMarca(cursor.getString(2));
        producto.setPrecio(cursor.getDouble(3));
        producto.setPredecedero(cursor.getInt(4));
        producto.setNo_predecedero(cursor.getInt(5));
        return producto;
    }

    public Producto getProducto(long id) {
        SQLiteDatabase db = productoDbHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLA_NAME,
                columnToRead,
                DefinirTabla.Producto._ID + " = ?",
                new String[] {String.valueOf(id)}, null, null, null );
        if(cursor.moveToFirst()) {
            Producto producto = readProducto(cursor);
            cursor.close();
            return producto;
        } else {
            return null;
        }

    }

    public ArrayList<Producto> allProductos() {
        ArrayList<Producto> productos = new ArrayList<Producto>();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLA_NAME,
                null, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Producto p = readProducto(cursor);
            cursor.moveToNext();
            productos.add(p);
        }
        cursor.close();
        return productos;

    }

}
