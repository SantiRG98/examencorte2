package database;

import android.provider.BaseColumns;

public class DefinirTabla {

    public DefinirTabla() {
    }

    public abstract class Producto implements BaseColumns{
        public static final String TABLA_NAME= "producto";
        public static final String NOMBRE= "nombre";
        public static final String MARCA= "marca";
        public static final String PRECIO= "precio";
        public static final String PREDECEDERO= "predecedero";
        public static final String NO_PREDECEDERO= "no_predecedero";
    }

}
